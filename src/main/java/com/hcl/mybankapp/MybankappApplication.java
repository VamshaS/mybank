package com.hcl.mybankapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MybankappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybankappApplication.class, args);
	}

}
