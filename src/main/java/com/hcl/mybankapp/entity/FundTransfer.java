package com.hcl.mybankapp.entity;

import java.time.LocalDate;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundTransfer {
	@Id
	private String fundTransferId;
	private long fromAccount;
	private long toAccount;
	private double amount;
	@CreationTimestamp
	private LocalDate transactionDate;
	private String otp;
	private String remarks;
	private boolean isVerified;
}
