package com.hcl.mybankapp.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {
	@Id
	private long accountId;
	private long accountNumber;
	private double accountBalance;
	private AccountType accountType;
	private LocalDate accountCreated;
	@OneToOne
	private Customer customer;
	
}
