package com.hcl.mybankapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BenificiaryAccount {
	private long benificiaryId;
	private String benificiaryName;
	private long accountNumber;
	private double balance;
	@ManyToOne
	private Customer customer;
}
