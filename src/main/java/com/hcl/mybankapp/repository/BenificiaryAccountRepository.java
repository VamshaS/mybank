package com.hcl.mybankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.mybankapp.entity.BenificiaryAccount;

public interface BenificiaryAccountRepository extends JpaRepository<BenificiaryAccount, Long> {

}
