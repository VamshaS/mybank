package com.hcl.mybankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.mybankapp.entity.FundTransfer;

public interface FundTransferRepository extends JpaRepository<FundTransfer, String>{

}
