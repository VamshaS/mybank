package com.hcl.mybankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountRepository, Long> {

}
