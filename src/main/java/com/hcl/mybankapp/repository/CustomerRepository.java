package com.hcl.mybankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.mybankapp.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,String>{

}
