package com.hcl.mybankapp.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.mybankapp.dto.OtpRequest;
import com.hcl.mybankapp.dto.OtpResponse;

@FeignClient(url = "https://prod-06.centralindia.logic.azure.com/workflows/a6cee14bdc1f4dfc95b1add34115b0e6/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=wdV2KuUywfZIYjr_DajgjrUkRKveUkGnqk7uBoGbreU", name = "otp-service")
public interface OtpClient {
	@PostMapping
	public OtpResponse getOtp(@RequestBody OtpRequest otpRequest);
}
