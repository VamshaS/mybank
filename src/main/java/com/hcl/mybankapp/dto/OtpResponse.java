package com.hcl.mybankapp.dto;

import lombok.Data;

@Data
public class OtpResponse {
	private String otp;
}
